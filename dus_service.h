//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "service.h"
#include "device.h"
#include "device_message.h"

#ifdef ESP8266
#include "Updater.h"
#endif

#include <FastCRC.h>

namespace marvin
{

FastCRC32 CRC32;

enum dus_error_code_t
{
  tmp_file_error = dus_error + 1,
  crc_error,
  frame_scrambled_error,
  update_package_oversize_error,
  md5_error,
  update_finalization_error,
  empty_data_error
};

bool transfer_started = false;
uint32_t current_frame = 0;

const char zero_ch = '0';
const char nine_ch = '9';
const char a_ch = 'a';
const char f_ch = 'f';
const char cap_a_ch = 'A';
const char cap_f_ch = 'F';

uint8_t get_char_value (const char* pchr)
{
  uint8_t val = 0;
  if ((*pchr >= zero_ch) &&
      (*pchr <= nine_ch))
  {
    val = *pchr - zero_ch;
  }
  else if ((*pchr >= a_ch) &&
           (*pchr <= f_ch))
  {
    val = *pchr - a_ch + 10;
  }
  else if ((*pchr >= cap_a_ch) &&
           (*pchr <= cap_f_ch))
  {
    val = *pchr - cap_a_ch + 10;
  }
  else
  {
    FLOG_ERROR(F("Char '%d' is not a valid hexadecimal value.\r\n"), *pchr);
  }

  return val;
}

size_t str2bin(uint8_t*& bin, const char* data_str)
{
  if (bin != nullptr)
  {
    delete bin;
  }

  if (data_str == nullptr)
  {
    return 0;
  }

  size_t len = strlen(data_str) / 2;
  bin = new uint8_t[len];

  for (size_t n = 0; n < len; ++n)
  {
    bin[n] = ((get_char_value(&data_str[n * 2]) & 0x0f) << 4 |
              (get_char_value(&data_str[(n * 2) + 1]) & 0x0f));
  }

 return len;
}

void handle_dus_service (const Message message)
{
  uint8_t destination_id = message->get_payload<uint8_t>(json_dest_id);
  int error_code = no_error;

  if (destination_id != get<uint8_t>(device_id))
  {
    return;
  }

  uint8_t source_id = message->get_payload<uint8_t>(json_source_id);
  service_state_t state = service_occupied(DUS, source_id);

  switch (state)
  {
  case state_occupied_other:
    {
      LOG_WARN("Another DUS request is active. Ignoring this request.");
      error_code = service_handler_occupied;
      break;
    }
  case state_free:
    {
      occupy_service(DUS, source_id);
      disable_interrupts();

      // Change device_status to maintenance
      // This will detach all interrupts (implemented in the sketch)
      // and send out a 252 message informing of this new state
      set<operational_status_t>(device_status, maintenance);

      transfer_started = false;
      current_frame = 0;

      // Identify whether this is a start-frame
      // Has json_data_size and json_md5?
      if (message->get_payload<size_t>(json_data_size) <= 0 ||
          !message->get_payload<const char*>(json_md5))
      {
        LOG_ERROR(F("DUS frames got scrambled!"));
        error_code = frame_scrambled_error;
        break;
      }

#ifdef ESP8266
      size_t data_size = message->get_payload<size_t>(json_data_size);
      FLOG_TRACE(F("Initiating update of %d bytes.\r\n\r\n"), data_size);
      if (!Update.begin(data_size))
      {
        LOG_ERROR("ERROR: Starting of OTA failed.");
        error_code = update_package_oversize_error;
        break;
      }

      if (!Update.setMD5(message->get_payload<const char*>(json_md5)))
      {
        LOG_ERROR("ERROR: MD5 invalid.");
        error_code = md5_error;
        break;
      }

      Update.runAsync(true); // @TODO Is this needed?
#endif

      break;
    }
  case state_occupied_self:
    {
      uint8_t* bin = nullptr;
      uint8_t* crc_bin = nullptr;

      do {
        // @TODO Identify whether this is a data-frame
        // Has json_data and json_data_crc?
        if (!message->get_payload<const char*>(json_data) ||
            !message->get_payload<const char*>(json_data_crc))
        {
          LOG_ERROR(F("DUS frames got scrambled!"));
          error_code = frame_scrambled_error;
          break;
        }

        uint32_t frame_no = message->get_payload<uint32_t>(json_frame_no);

        if (!transfer_started)
        {
          if (frame_no != 0)
          {
            LOG_ERROR(F("DUS data frame not staring wuth zero!"));
            error_code = frame_scrambled_error;
            break;
          }

          transfer_started = true;
        }
        else
        {
          if (current_frame == frame_no)
          {
            // When the previous frame is resent, we just ignore and confirm it
            // Most probabliy the counterpart did not receive our acknowledgment
            break;
          }

          if (frame_no != 1 + current_frame)
          {
            LOG_ERROR(F("DUS data frame lost!"));
            error_code = frame_scrambled_error;
            break;
          }
        }

        current_frame = frame_no;

        const char * data = message->get_payload<const char*>(json_data);
        size_t length_decoded = str2bin(bin, data);

        const char * data_crc_str = message->get_payload<const char*>(json_data_crc);
        size_t length_decoded_crc = str2bin(crc_bin, data_crc_str);
        uint32_t data_crc = 0;

        if (length_decoded_crc != 4)
        {
          FLOG_ERROR(F("CRC32 calculation did not return 4 bytes but %d bytes!\r\n"), length_decoded_crc);
          error_code = internal_state_error;
          break;
        }

        for (uint8_t i = 0; i < 4; ++i)
        {
          data_crc |= crc_bin[3 - i] << (8 * i);
        }

        uint32_t calculated_crc = CRC32.crc32(bin, length_decoded);

        if (calculated_crc != data_crc)
        {
          FLOG_ERROR(F("CRC32 missmatch! %d (frame) != %d (calculated)\r\n"), data_crc, calculated_crc);
          error_code = crc_error;
          break;
        }

  #ifdef ESP8266
        if (length_decoded > 0)
        {
          size_t written = Update.write(bin, length_decoded);
          if (length_decoded != written)
          {
            FLOG_ERROR(F("Could not write all expected %d Bytes of frame no. %d to flash. %d Bytes written\r\n"), length_decoded, frame_no, written);
            error_code = tmp_file_error;
          }
        }
        else
        {
          error_code = empty_data_error;
        }
  #endif
      } while (false);

      if (bin)
      {
        delete bin;
      }

      if (crc_bin)
      {
        delete crc_bin;
      }

      if (error_code == no_error &&
          0 == message->get_payload<uint8_t>(json_expect_more))
      {
#ifdef ESP8266
        if (!Update.end())
        {
          LOG_INFO(F("Finalizing the DUS update (normal case) caused an error."));
          Update.printError(Serial);
        }
#endif
        current_frame = 0;
        enable_interrupts();
        free_service(DUS, source_id);
      }
      break;
    }
  default:
    {
      FLOG_ERROR("ERROR: Invalid Service state: '%d'.\r\n", state);
      error_code = internal_state_error;
      break;
    }
  }

  Message result = Message(new DeviceMessage(get<uint8_t>(device_id),
                                             message->message_id + 1));
  result->set_payload(json_error_code, error_code);
  send(result);

  if (error_code != no_error &&
      error_code != crc_error &&
      error_code != service_handler_occupied)
  {
    free_service(DUS, source_id);
#ifdef ESP8266
    if (!Update.end())
    {
      LOG_ERROR(F("Finalizing the DUS update (error case) caused an error."));
      Update.printError(Serial);
    }
#endif
    current_frame = 0;
    enable_interrupts();
  }
}

}
