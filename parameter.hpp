//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once


#include "Arduino.h"
#include "globals.h"
#include "filesystem.h"

#include "object_parser.hpp"

#include <forward_list>

namespace marvin
{

class ParameterBase
{
public:

  virtual const_string to_string() const = 0;
  virtual void from_string(const char* value, bool store = true) = 0;

  void from_string(const_string& value, bool store = true)
  {
    from_string(value.get(), store);
  }

  void load();
  void store();
  void initialize();

  template <typename T>
  static
  void register_element (const char* name, uint16_t param_id, const char* description, const T& initial_value, bool persistent);

  static
  bool is_modified(uint16_t param);

  static
  void reset_modified(uint16_t param);

  static
  void dump_parameters();

  static
  ParameterBase* find_parameter(uint16_t param);

  static
  ParameterBase* find_parameter_by_name(const_string& param)
  {
    return ParameterBase::find_parameter_by_name(param.get());
  }

  static
  ParameterBase* find_parameter_by_name(const char* param);

  virtual ~ParameterBase()
  {
    if (name)
    {
      delete name;
      name = nullptr;
    }

    if (description)
    {
      delete description;
      description = nullptr;
    }
  }

  const uint16_t id;
  const char* name = NULL;
  const char* description = NULL;
  bool persistent;
  bool modified;

protected:
  ParameterBase(const uint16_t id, const char* name, const char* description, bool persistent)
  : id(id), name(name), description(description), persistent(persistent), modified(false)
  {}

private:
  static std::forward_list<ParameterBase*> m_parameter_table;
};

template <typename T>
class Parameter : public ParameterBase
{
public:

  Parameter(const uint16_t id, const char* name, const char* description,
            bool persistent, const T& initial_value) :
    ParameterBase(id, name, description, persistent), value(cloner<T>(initial_value))
  {}

  ~Parameter()
  {
    liberate(value);
  }

  virtual const_string to_string() const
  {
    return stringify<T>(this->value);
  }

  virtual void from_string(const char* value, bool store = true)
  {
    this->value = read<T>(value);

    if (store && persistent)
    {
      this->store();
    }
  }

  T value;
};

template <typename T>
void ParameterBase::register_element (const char* name, uint16_t param_id, const char* description, const T& initial_value, bool persistent)
{
  ParameterBase* p = new Parameter<T>(param_id, name, description, persistent, initial_value);
  p->initialize();
  m_parameter_table.push_front(p);
}


template <typename T>
IRAM_ATTR
const T& get(uint16_t param, bool reset_modified = false)
{
  ParameterBase* p = ParameterBase::find_parameter(param);

  if (reset_modified && p->modified)
  {
    p->modified = false;
  }

  const Parameter<T>* self = reinterpret_cast<const Parameter<T>*>(p); // dynamic_cast is not avaliable on Arduino
  return self->value;
}


template <typename T>
IRAM_ATTR
bool set(uint16_t param, const T& value)
{
  ParameterBase* p = ParameterBase::find_parameter(param);
  if (p != nullptr)
  {
    Parameter<T>* self = reinterpret_cast<Parameter<T>*>(p);
    liberate<T>(self->value);
    self->value = cloner<T>(value);
    self->modified = true;
    if (self->persistent)
    {
      self->store();
    }
    return true;
  }
  else
  {
    return false;
  }
}

template <typename T>
inline
bool set_iff(uint16_t param, const T& value)
{
  return (get<T>(param) == value ||
          set<T>(param, value));
}

}
