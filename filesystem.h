//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "Arduino.h"


#ifndef ESP8266
namespace marvin
{

class File
{
public:
  const char* read_until (char terminal);
  const char* read_line ();
  size_t write (const char* data);

  bool operator! ()
  {
    return false;
  }

  bool close ();
};

class FileSystem
{
public:
  bool begin();
  bool format();

  File open (char* path, char* mode);
  File open (const StringSumHelper& path, char* mode);
  bool exists (char* path);
  bool exists (const StringSumHelper& path);
  bool remove (char* path);
  bool remove (const StringSumHelper& path);

private:
};

}

extern marvin::FileSystem FS;
#else
#include <FS.h>
#include <LittleFS.h>
#define MARVINFS LittleFS
#endif
