//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "service.h"
#include "device.h"
#include "device_message.h"

namespace marvin
{

enum pis_error_code_t
{
  parameter_not_found = pis_error + 1
};

void handle_pis_service (const Message message)
{
  uint8_t destination_id = message->get_payload<uint8_t>(json_dest_id);

  if (destination_id != get<uint8_t>(device_id))
  {
    return;
  }

  const_string data = const_string();
  uint8_t stat = no_error;

  ParameterBase* p = ParameterBase::find_parameter_by_name(message->get_payload<const char*>(json_parameter));

  if (p == nullptr)
  {
    stat = pis_error + parameter_not_found;
    FLOG(F("Parameter '%s' is not registered.\r\n"), message->get_payload<const char*>(json_parameter));
    data = const_string(cloner(parameter_not_found_str));
  }
  else
  {
    if (message->get_payload<bool>(json_read_data))
    {
      data = p->to_string();
      FLOG("Returning value '%s' for parameter '%s'.\r\n", data, message->get_payload<const char*>(json_parameter));
    }
    else
    {
      FLOG("Setting parameter '%s' to value '%s.'\r\n", message->get_payload<const char*>(json_parameter), message->get_payload<const char*>(json_data));

      p->from_string(message->get_payload<const char*>(json_data));
      p->modified = true;
      data = p->to_string();
    }
  }

  Message response = Message(new DeviceMessage(get<uint8_t>(device_id),
					       message->message_id + 1));
  response->set_payload(json_data, data.get());
  response->set_payload(json_status, stat);

  send(response);
}

}
