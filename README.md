

Compile using Arduino CLI

Example
arduino-cli compile --fqbn esp8266:esp8266:generic --libraries="./libraries" -v devices/env_internal_sensor/env_internal_sensor.ino


Needed libraries

DHT_sensor_library @ 1.4.3
LittleFS @ 0.1.0
ArduinoJson @ 6.19.4
Adafruit_Unified_Sensor @ 1.1.5
EEPROM @ 1.0
FastCRC @ 1.42
ESP8266WiFi @ 1.0
ESP8266WebServer @ 1.0
DNSServer @ 1.1.1
WebSockets2_Generic @ 1.10.3

Note: WebSockets2_Generic is only needed when MARVIN_WEBSOCKETS is defined true.
