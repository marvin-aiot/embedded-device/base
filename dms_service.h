//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "service.h"
#include "device.h"
#include "device_message.h"

namespace marvin
{

enum dms_error_code_t
{
  file_not_found_error = dms_error + 1
};

void handle_dms_service (const Message message)
{
  uint8_t destination_id = message->get_payload<uint8_t>(json_dest_id);
  if (destination_id != get<uint8_t>(device_id))
  {
    return;
  }

  uint8_t ack = no_error;
  bool reboot_needed = false;
  uint8_t source_id = message->get_payload<uint8_t>(json_source_id);
  service_state_t state = service_occupied(DMS, source_id);

  switch (state)
  {
  case state_occupied_other:
    {
      LOG(F("Another DMS request is active. Ignoring this request."));
      ack = service_handler_occupied;
      break;
    }
  case state_free:
    {
      occupy_service(DMS, source_id);
      // no break here, we continue handling as if state_occupied_self
      [[fallthrough]];
    }
  case state_occupied_self:
    {
      uint8_t command = message->get_payload<uint8_t>(json_command);
      FLOG(F("DMS command: %d\r\n"), command);

      switch (command)
      {
        case 0:
        {
          set<uint8_t>(restart_counter, 0);
          reboot_needed = true;
          LOG(F("Normal reboot requested."));
          break;
        }
        case 1:
        {
          set<uint8_t>(restart_counter, 3);
          reboot_needed = true;
          LOG(F("Maintenance reboot requested."));
          break;
        }
        case 2:
        {
          const char * argument = message->get_payload<const char*>(json_argument);
          if (MARVINFS.exists(argument))
          {
            MARVINFS.remove(argument);
          }
          else
          {
            ack = file_not_found_error;
          }

          break;
        }
        default:
        {
          FLOG(F("Invalid DMS command: %d\r\n"), command);
          ack = invalid_operation;
          break;
        }
      }
      break;
    }
  default:
    {
      FLOG(F("ERROR: Invalid Service state: '%d'.\r\n"), state);
      return;
    }
  }

  Message response = Message(new DeviceMessage(get<uint8_t>(device_id),
					         message->message_id + 1));

  response->set_payload(json_ack, ack);
  send(response);

  if (reboot_needed)
  {
    restart();
  }
  else
  {
    free_service(DMS, source_id);
  }
}

}
