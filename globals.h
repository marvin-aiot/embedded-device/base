//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <memory>

namespace marvin
{

#ifndef MARVIN_VERBOSITY_LEVEL
/*
 * Log Levels
 * 0 - OFF (default for production)
 * 1 - ERROR
 * 2 - WARN
 * 3 - INFO (default for development)
 * 4 - TRACE
 * 5 - DEBUG
 */ 
#define MARVIN_VERBOSITY_LEVEL 3
#endif

typedef std::unique_ptr<char[]> string;
typedef std::unique_ptr<const char[]> const_string;


extern const char serial_number_str[17];
extern uint64_t serial_number_int; // @Note Serial Number is actually 7 bytes
extern const char device_pin_str[9];
extern uint32_t device_pin_int;

bool interrupts_enabled(); // OTA update crashes whenever interrupt handlers try to set a parameter, so we disable them
void enable_interrupts();
void disable_interrupts();

#if MARVIN_VERBOSITY_LEVEL > 0

  template<class T>
  inline
  const T& convert_for_log(T const& x)
  {
      return x;
  }

  inline
  const char* convert_for_log(const_string& str)
  {
      return str.get();
  }

  inline
  const char* convert_for_log(const String& str)
  {
      return str.c_str();
  }

  template<typename T>
  inline
  void LOG(const T& format)
  {
      Serial.println(convert_for_log(format));
  }

  template<class...Args>
  inline
  void FLOG(const char* format, Args&&...args)
  {
      Serial.printf(format, convert_for_log(args)...);
  }

  template<class...Args>
  void FLOG(const __FlashStringHelper* format, Args&&...args)
  {
    size_t len = strlen_P((PGM_P)format);
    char* val = new char[len + 1];
    memcpy_P(val, format, len);
    val[len] = '\0';
    Serial.printf(val, convert_for_log(args)...);
    delete val;
  }

  template<typename T, class...Args>
  inline
  void ILOG(const T& format, Args&&...args)
  {
    FLOG(format, args ...);
  }

#else
  #define ILOG(...) {}
  #define FLOG(...) {}
  #define LOG(...) {}
#endif

extern const char* json_ack PROGMEM;
extern const char* json_argument PROGMEM;
extern const char* json_command PROGMEM;
extern const char* json_counter PROGMEM;
extern const char* json_data PROGMEM;
extern const char* json_data_crc PROGMEM;
extern const char* json_data_size PROGMEM;
extern const char* json_dest_id PROGMEM;
extern const char* json_dest_sn PROGMEM;
extern const char* json_error_code PROGMEM;
extern const char* json_expect_more PROGMEM;
extern const char* json_frame_no PROGMEM;
extern const char* json_ip_address PROGMEM;
extern const char* json_mac_address PROGMEM;
extern const char* json_md5 PROGMEM;
extern const char* json_message_id PROGMEM;
extern const char* json_new_id PROGMEM;
extern const char* json_offset PROGMEM;
extern const char* json_parameter PROGMEM;
extern const char* json_parameters PROGMEM;
extern const char* json_payload PROGMEM;
extern const char* json_occupied PROGMEM;
extern const char* json_read_data PROGMEM;
extern const char* json_rtr PROGMEM;
extern const char* json_serial_number PROGMEM;
extern const char* json_status PROGMEM;
extern const char* json_source_id PROGMEM;

extern const char* parameter_not_found_str;

// These parameters have to be registered in the setup function (see device.cpp)
enum global_device_parameters
{
  device_id = 9000,
  device_status,
  device_run_level
};

enum run_level_t
{
    RL0 = 0,
    RL1,
    RL2
};

enum operational_status_t // @REVIEW There should be a separation between device and bus status
{
  power_up = 0,

  offline = 6,
  online = 7,
  uncommissioned = 8,
  recovery = 9,
  maintenance = 10,
  operational = 20,

  locked = 90
};

enum service_t
{
  DMS = 960,
  DUS = 962,
  ISS = 964,
  PIS = 966,
  
  NUM_SERVICES = 4
};

enum service_state_t
{
  state_free,
  state_occupied_self,
  state_occupied_other
};

enum error_type_t
{
  no_error = 0,
  invalid_message_id = 1,
  invalid_message_type = 2,
  service_handler_occupied = 3,
  invalid_operation = 4,
  internal_state_error = 9,
  dis_error = 10,
  dds_error = 20,
  dus_error = 30,
  iss_error = 40,
  pis_error = 50,
  dms_error = 60
};



#if MARVIN_VERBOSITY_LEVEL == 0
#define MARVIN_VERBOSITY_ERROR false
#define MARVIN_VERBOSITY_WARN  false
#define MARVIN_VERBOSITY_INFO false
#define MARVIN_VERBOSITY_TRACE false
#define MARVIN_VERBOSITY_DEBUG false
#endif
  
#if MARVIN_VERBOSITY_LEVEL == 1
#define MARVIN_VERBOSITY_ERROR true
#define MARVIN_VERBOSITY_WARN  false
#define MARVIN_VERBOSITY_INFO false
#define MARVIN_VERBOSITY_TRACE false
#define MARVIN_VERBOSITY_DEBUG false
#endif

#if MARVIN_VERBOSITY_LEVEL == 2
#define MARVIN_VERBOSITY_ERROR true
#define MARVIN_VERBOSITY_WARN  true
#define MARVIN_VERBOSITY_INFO false
#define MARVIN_VERBOSITY_TRACE false
#define MARVIN_VERBOSITY_DEBUG false
#endif

#if MARVIN_VERBOSITY_LEVEL == 3
#define MARVIN_VERBOSITY_ERROR true
#define MARVIN_VERBOSITY_WARN  true
#define MARVIN_VERBOSITY_INFO true
#define MARVIN_VERBOSITY_TRACE false
#define MARVIN_VERBOSITY_DEBUG false
#endif

#if MARVIN_VERBOSITY_LEVEL == 4
#define MARVIN_VERBOSITY_ERROR true
#define MARVIN_VERBOSITY_WARN  true
#define MARVIN_VERBOSITY_INFO true
#define MARVIN_VERBOSITY_TRACE true
#define MARVIN_VERBOSITY_DEBUG false
#endif
  
#if MARVIN_VERBOSITY_LEVEL == 5
#define MARVIN_VERBOSITY_ERROR true
#define MARVIN_VERBOSITY_WARN  true
#define MARVIN_VERBOSITY_INFO true
#define MARVIN_VERBOSITY_TRACE true
#define MARVIN_VERBOSITY_DEBUG true
#endif

/*
#define xcat__(a,b)  a##b
#define concat__(...) xcat__(__VA_ARGS__)
#define eval__(x) x

#define define_log_x_true(level) bool LOG_##level(format) { LOG(format); }
#define define_ilog_x_true(level) bool ILOG_##level(format, args...) { ILOG(format, args); }
#define define_flog_x_true(level) bool FLOG_##level(format, args...) { FLOG(format, args); }

#define define_log_x_false(level) bool LOG_##level(format) { }
#define define_ilog_x_false(level) bool ILOG_##level(format, args...) { }
#define define_flog_x_false(level) bool FLOG_##level(format, args...) { }

#define define_logger (level) concat__(define_log_x_, eval__(concat__(MARVIN_VERBOSITY_, level)))

define_logger(ERROR)
*/

#if MARVIN_VERBOSITY_ERROR
#define LOG_ERROR(format) LOG(format)
#define ILOG_ERROR(format, args...) ILOG(format, ##args)
#define FLOG_ERROR(format, args...) FLOG(format, ##args)
#else
#define LOG_ERROR(...)
#define ILOG_ERROR(...)
#define FLOG_ERROR(...)
#endif

#if MARVIN_VERBOSITY_WARN
#define LOG_WARN(format) LOG(format)
#define ILOG_WARN(format, args...) ILOG(format, ##args)
#define FLOG_WARN(format, args...) FLOG(format, ##args)
#else
#define LOG_WARN(...)
#define ILOG_WARN(...)
#define FLOG_WARN(...)
#endif

#if MARVIN_VERBOSITY_INFO
#define LOG_INFO(format) LOG(format)
#define ILOG_INFO(format, args...) ILOG(format, ##args)
#define FLOG_INFO(format, args...) FLOG(format, ##args)
#else
#define LOG_INFO(...)
#define ILOG_INFO(...)
#define FLOG_INFO(...)
#endif

#if MARVIN_VERBOSITY_TRACE
#define LOG_TRACE(format) LOG(format)
#define ILOG_TRACE(format, args...) ILOG(format, ##args)
#define FLOG_TRACE(format, args...) FLOG(format, ##args)
#else
#define LOG_TRACE(...)
#define ILOG_TRACE(...)
#define FLOG_TRACE(f...)
#endif

#if MARVIN_VERBOSITY_DEBUG
#define LOG_DEBUG(format) LOG(format)
#define ILOG_DEBUG(format, args...) ILOG(format, ##args)
#define FLOG_DEBUG(format, args...) FLOG(format, ##args)
#else
#define LOG_DEBUG(...)
#define ILOG_DEBUG(...)
#define FLOG_DEBUG(...)
#endif

}
