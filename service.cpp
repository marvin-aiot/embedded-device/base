
#include "Arduino.h"

#include "globals.h"
#include "object_parser.hpp"
#include "service.h"

namespace marvin
{

uint8_t m_occupied_services[NUM_SERVICES] = {0};

uint8_t convert_msg_id_to_1p_index (service_t service)
{
  if (DMS > service || service > PIS)
  {
    FLOG(F("Unvalid service message ID '%d'.\r\n"), service);
    return 0;
  }

  return 1 + (service - DMS) / 2;
}

bool occupy_service(service_t service, uint8_t source_id)
{
  bool res = false;
  uint8_t idx = convert_msg_id_to_1p_index(service);
  if (idx != 0)
  {
    if(0 == m_occupied_services[idx - 1])
    {
      m_occupied_services[idx - 1] = source_id;
      res = true;
    }
    else
    {
      FLOG(F("Unable to occupy Service '%d'. It is occupied by another UID ('%d' != '%d').\r\n"),
      service, m_occupied_services[idx - 1], source_id);
    }
  }

  return res;
}

void free_service(service_t service, uint8_t source_id)
{
  uint8_t idx = convert_msg_id_to_1p_index(service);
  if (idx != 0)
  {
    if (m_occupied_services[idx - 1] == source_id)
    {
      m_occupied_services[idx - 1] = 0;
    }
    else
    {
      FLOG(F("Unable to free Service '%d'. It is occupied by another UID ('%d' != '%d').\r\n"),
        service, m_occupied_services[idx - 1], source_id);
    }
  }
}

service_state_t service_occupied(service_t service, uint8_t source_id)
{
  uint8_t idx = convert_msg_id_to_1p_index(service);
  service_state_t state = state_occupied_other;

  if (idx != 0)
  {
    if (0 == m_occupied_services[idx - 1])
    {
      state = state_free;
    }
    else if (m_occupied_services[idx - 1] == source_id)
    {
      state = state_occupied_self;
    }
  }

  return state;
}

}
