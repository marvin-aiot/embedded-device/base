

#ifdef ESP8266

  #define AIN A0
  #define GPIO0 9
  #define GPIO1 10
  #define GPIO2 14
  #define GPIO3 12
  #define GPIO4 13
  #define GPIO5 2
  #define GPIO6 4
  #define GPIO7 5

#else

  #define AIN PA0
  #define GPIO0 PA1
  #define GPIO1 PA2
  #define GPIO2 PA3
  #define GPIO3 PB0
  #define GPIO4 PB1
  #define GPIO5 PB2
  #define GPIO6 PB6
  #define GPIO7 PB7

#endif
