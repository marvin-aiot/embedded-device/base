

#ifdef ESP8266

#include "Arduino.h"
#include "globals.h"
#include "bus.h"
#include "device.h"
#include "parameter.hpp"
#include "device_message.h"
#include "scheduler.h"

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <forward_list>

// ---- Use a WebSocket connection to transmit messages
#define MARVIN_WEBSOCKETS false

// ---- Send multiple copies of each message to ensure reception
#define MESSAGE_REDUNDANCY false

// ---- Enable the Arduino OTA functionality
#define OTA_ENABLED false

#if MARVIN_WEBSOCKETS
#define _WEBSOCKETS_LOGLEVEL_ 0
#include <WebSockets2_Generic.h>
//#include <ArduinoWebsockets.h>
using namespace websockets2_generic;
WebsocketsClient ws_client;
#endif

#if OTA_ENABLED
#include <ArduinoOTA.h>
#endif

#define WIFI_CONNECT_TIMEOUT 15000
#define WIFI_RETRY_INTERVAL 45000

//#ifdef UDP_TX_PACKET_MAX_SIZE
//  #define RX_PACKET_MAX_SIZE UDP_TX_PACKET_MAX_SIZE
//#else
  #define RX_PACKET_MAX_SIZE 1260
//#endif


namespace marvin
{
Message parse_message(const char * packet);

char packet_buffer[RX_PACKET_MAX_SIZE]; //buffer to hold incoming packet

const uint16_t local_port = 4242;

const uint16_t ap_ssid = 1000;
const uint16_t ap_passwd = 1001;
const uint16_t network_master_addr = 1002;
const uint16_t network_master_ws_port = 1003;

task_id_t connection_establish_task_id = 0;
task_id_t try_reconnect_task_id = 0;
task_id_t clean_monitored_messages_task_id = 0;
uint8_t connection_counter = 0;
bool wait = false;
uint8_t message_counter = 0;

// @REVIEW Move this string into FS
const char* wifi_htm PROGMEM = R"=====(
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <title>Marvin: Embedded Device Wifi Setup</title>
    <style>
      input{
        padding:5px;font-size:1em;width:95%;
      }
      body{
        text-align:center;font-family:verdana;background-color:black;color:white;
      }
      a{
        color:#1fa3ec;
      }
      button{
        border:0;border-radius:0.3em;background-color:#1fa3ec;color:#fff;
        line-height:2.4em;font-size:1.2em;width:100%;display:block;
      }
      .q{
        float:right;
      }
      .s{
        display:inline-block;width:14em;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;
      }
    </style>
  </head>
  <body>
    <div style='text-align:left;display:inline-block;width:320px;padding:5px'>
      <form method='post' action='/connect'>
        <input id='s' name='s' length=32 placeholder='SSID'>
        <br>
        <input id='p' name='p' length=64 type='password' placeholder='password'>
        <br><br>
        <button type='submit'>Connect</button>
      </form>
    </div>
  </body>
</html>
)=====";

WiFiUDP udp_socket;
IPAddress broadcast_ip;
WiFiEventHandler disconnected_event_handler;
ESP8266WebServer* server = nullptr;
DNSServer* dns_server = nullptr;

typedef std::tuple<uint8_t /* source_id */,
                   uint16_t /* message_id */,
                   uint8_t /* message counter*/,
                   bool /* rtr */,
                   uint32_t /* timestamp */> MonitoredMessage;
typedef std::forward_list<MonitoredMessage> MonitoredMessagesList;

static MonitoredMessagesList monitored_messages = {};
std::forward_list<MonitoredMessage> messages_to_remove = {};
static const uint32_t time_to_live = 300;

inline
bool equal_monitored_message (const MonitoredMessage& l, const MonitoredMessage& r)
{
  bool ret = std::get<0>(l) ==  std::get<0>(r) &&
             std::get<1>(l) ==  std::get<1>(r) &&
             std::get<2>(l) ==  std::get<2>(r) &&
             std::get<3>(l) ==  std::get<3>(r) &&
             std::get<4>(l) ==  std::get<4>(r);
  return ret;
}

IRAM_ATTR
void clean_monitored_messages(task_id_t, void*)
{
  uint32_t now_ts = millis();
  messages_to_remove.clear();
#if MARVIN_VERBOSITY_TRACE
  uint32_t message_counter = 0;
#endif
  for(const MonitoredMessage& msg : monitored_messages)
  {
    const uint32_t & timestamp = std::get<4>(msg);
#if MARVIN_VERBOSITY_TRACE
    ++message_counter;
#endif
    if (now_ts - time_to_live > timestamp)
    {
      messages_to_remove.push_front(std::make_tuple(std::get<0>(msg), std::get<1>(msg), std::get<2>(msg),
                                                    std::get<3>(msg), std::get<4>(msg)));
    }
  }

#if MARVIN_VERBOSITY_TRACE
  uint32_t remove_counter = 0;
#endif

  for (const MonitoredMessage& msg : messages_to_remove)
  {
    monitored_messages.remove_if ([&msg](const MonitoredMessage& elm)
                                  {
                                    return equal_monitored_message(msg, elm);
                                  });
#if MARVIN_VERBOSITY_TRACE
    ++remove_counter;
#endif
  }

#if MARVIN_VERBOSITY_TRACE
  if (remove_counter > 0 || message_counter > 0)
  {
    FLOG(F("%d redundant messages removed. Now remains %d.\r\n"), remove_counter, message_counter - remove_counter);
  }
#endif
}

IRAM_ATTR
bool is_recurrent_message(const Message & message)
{
  bool found = false;

  // Check for repeated Message-Id & Source-Id
  // Rationale: To ensure successful transmission of UPD packets,
  // messages should be sent three times in an intervall of 100ms
  uint32_t now_ts = millis();
  for(const MonitoredMessage& msg : monitored_messages)
  {
    const uint8_t & source_id = std::get<0>(msg);
    const uint16_t & message_id = std::get<1>(msg);
    const uint8_t & counter = std::get<2>(msg);
    const bool & rtr = std::get<3>(msg);

    if (source_id == message->source_id &&
        message_id == message->message_id &&
        rtr == message->rtr &&
        counter == message->counter &&
        std::get<3>(msg) + time_to_live > now_ts)
    {
      found = true;
      break;
    }
  }

  if (!found)
  {
    monitored_messages.push_front(std::make_tuple(message->source_id, message->message_id, message->counter,
                                                  message->rtr, now_ts));
  }

  return found;
}

#if MARVIN_WEBSOCKETS
WebsocketsClient ws_client;
bool ws_connected = false;
const char* subscribe_format PROGMEM = "{\":subscription-action\": \"subscribe\", \":device_id\": %d, \":message_id\": %d}";
void handle_network_master_changed(task_id_t task_id, void* args);

void onEventsCallback(WebsocketsEvent event, String data)
{
  (void) data;

  if (event == WebsocketsEvent::ConnectionOpened)
  {
    ws_connected = true;
    LOG(F("WS Connnection Opened"));

    char json_subscribe[65];
    call_on_relevant_messages([&](uint8_t dev_id, uint16_t msg_id)
    {
      // Subscribe via WS to (dev_id, msg_id)
      snprintf(json_subscribe, 64, subscribe_format, dev_id, msg_id);

      ILOG(F("WS Subscribing: "));
      LOG(json_subscribe);

      if (ws_connected)
      {
        ws_client.send(json_subscribe);
      }
    });
  }
  else if (event == WebsocketsEvent::ConnectionClosed)
  {
    ws_connected = false;
    LOG(F("WS Connnection Closed"));

    // Try and reconnect to WS
    handle_network_master_changed(0, nullptr);

  }
  else if (event == WebsocketsEvent::GotPing)
  {
    LOG(F("Got a WS Ping!"));
  }
  else if (event == WebsocketsEvent::GotPong)
  {
    LOG(F("Got a WS Pong!"));
  }
}

void handle_network_master_changed(task_id_t task_id, void* args)
{
  (void) task_id;
  (void) args;

  ParameterBase::reset_modified(network_master_addr);
  ParameterBase::reset_modified(network_master_ws_port);

  if (strlen(get<const char*>(network_master_addr)) == 0)
  {
    if (ws_connected)
    {
      ws_client.close();
      ws_connected = false;
    }

    return;
  }

  // run callback when messages are received
  ws_client.onMessage([&](WebsocketsMessage ws_message)
  {
    ILOG(F("Got Message: "));
    LOG(ws_message.data());

    Message message = parse_message(&ws_message.data()[0]);

    if (!is_recurrent_message(message))
    {
      handle(message);
    }
  });

  // run callback when events are occuring
  ws_client.onEvent(onEventsCallback);

  // try to connect to Websockets server
  bool connected = ws_client.connect(get<const char*>(network_master_addr), get<uint16_t>(network_master_ws_port), "/ws-network");

  if (connected)
  {
    LOG(F("WS connected!"));
    ws_connected = true;
  }
  else
  {
    LOG(F("WS not connected!"));
    // Try again in 30 seconds
    ws_connected = false;
  }
}
#endif

void setup_ota()
{
#if OTA_ENABLED
  LOG(F("Setting up fallback OTA service."));
  ArduinoOTA.setHostname(serial_number_str);
  ArduinoOTA.setPassword(device_pin_str);

  // ArduinoOTA.setPort(4243);   // @REVIEW Can the Arduino IDE handle a different port?

  ArduinoOTA.onStart(
    []()
    {
      LOG(F("Starting an update."));
      // We must inhibit all interrupts defined by the user and the framework. This can force a crash!
      disable_interrupts();
    });

  ArduinoOTA.onEnd(
    []()
    {
      LOG(F("Update ended."));
    });

  ArduinoOTA.onError(
    [](ota_error_t error)
    {
      if (error == OTA_AUTH_ERROR)
      {
        LOG(F("Auth Failed"));
      }
      else if (error == OTA_BEGIN_ERROR)
      {
        LOG(F("Begin Failed"));
      }
      else if (error == OTA_CONNECT_ERROR)
      {
        LOG(F("Connect Failed"));
      }
      else if (error == OTA_RECEIVE_ERROR)
      {
        LOG(F("Receive Failed"));
      }
      else if (error == OTA_END_ERROR)
      {
        LOG(F("End Failed"));
      }
      else
      {
        LOG(F("Unknown error!"));
      }

      ESP.restart();
    });

  ArduinoOTA.begin();
#endif
}

void handle_ota()
{
#if OTA_ENABLED
  ArduinoOTA.handle();
#endif
}

inline
bool can_send(uint8_t msg_id)
{
  // @REVIEW We would like to cache device_id per onchange task, but ISS has to respond to setting a Device ID before the update task can run.
  return WiFi.status() == WL_CONNECTED &&
          (get<uint8_t>(device_id) != 0 ||
           msg_id == 252);
}

void connection_establish_task(task_id_t task_id, void* args)
{
  (void) task_id;
  (void) args;

  if (WiFi.status() != WL_CONNECTED)
  {
    // If this takes too long, enter recovery.
    if ((connection_counter > (WIFI_CONNECT_TIMEOUT / 500) ||
         WiFi.status() == WL_NO_SSID_AVAIL ||
         WiFi.status() == WL_CONNECT_FAILED) &&
         get<operational_status_t>(device_status) != recovery)
    {
      set<operational_status_t>(device_status, recovery);
      disable_task(clean_monitored_messages_task_id);
      disable_task(connection_establish_task_id);

      if (connection_counter > (WIFI_CONNECT_TIMEOUT / 500))
      {
        LOG(F("Taking too long to connect. Starting AP."));
      }
      else
      {
        LOG(F("There was an error with the connection credentials. Starting AP."));
      }

      ++connection_counter;
    }
  }
  else
  {
    disconnected_event_handler = WiFi.onStationModeDisconnected([](const WiFiEventStationModeDisconnected& event)
    {
      (void) event;
      set<operational_status_t>(device_status, offline);
      disable_task(clean_monitored_messages_task_id);
      LOG(F("Station disconnected!"));
    });

    FLOG(F("\r\nWiFi connected\r\nIP address: %s\r\nMAC: %s\r\n"), WiFi.localIP().toString(), WiFi.macAddress());

    broadcast_ip = ~(uint32_t)WiFi.subnetMask() | (uint32_t)WiFi.gatewayIP();

    set<operational_status_t>(device_status, online);

    enable_task(clean_monitored_messages_task_id);
    disable_task(connection_establish_task_id);

    setup_ota();
  }
}

void try_reconnect_task(task_id_t task_id, void* args)
{
  // It is possible, that the AP just restarted and is visible again.
  // In this case we must continue the normal operation after it reconnected.
  (void) task_id;
  (void) args;

  if (strlen(get<const char*>(ap_ssid)) > 0)
  {
    if (wait)
    {
      LOG(F("Giving up to reconnect to Acess Point."));
      WiFi.disconnect();
      reschedule_task(millis() + WIFI_RETRY_INTERVAL);
    }
    else
    {
      LOG(F("Trying to reconnect to Acess Point."));
      WiFi.begin(get<const char*>(ap_ssid), get<const char*>(ap_passwd));
      reschedule_task(millis() + WIFI_CONNECT_TIMEOUT);
    }

    wait = !wait;
  }
}

void handle_credentials_changed(task_id_t task_id, void* args)
{
  (void) task_id;
  (void) args;

  ParameterBase::reset_modified(ap_ssid);
  ParameterBase::reset_modified(ap_passwd);

  // Not checking whether both ssid and passwd have been changed can create a situation 
  // were a new ssid was writen (via PIS) but disconnected before the new passwd could be sent.

  // Ignore further calls while a connection is already being established
  if (task_enabled(connection_establish_task_id))
  {
    return;
  }

  WiFi.mode(WIFI_OFF);
  WiFi.persistent(false);
  delay(1);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  if (get<operational_status_t>(device_status) != offline)
  {
    set<operational_status_t>(device_status, offline);
  }

  if (strlen(get<const char*>(ap_ssid)) > 0 &&
      strlen(get<const char*>(ap_passwd)) > 0)
  {
    FLOG(F("Connecting to '%s' with password '%s'.\r\n"),
      get<const char*>(ap_ssid), get<const char*>(ap_passwd));

    WiFi.begin(get<const char*>(ap_ssid), get<const char*>(ap_passwd));

    connection_counter = 0;
    enable_task(connection_establish_task_id);
  }
  else
  {
    LOG(F("No SSID/Password configured. Starting AP."));
    set<operational_status_t>(device_status, recovery);
  }
}

bool bus_setup ()
{
  // Start UDP socket
  udp_socket.begin(local_port);

  ParameterBase::register_element<const char*>  ("ap_ssid",                ap_ssid,                "",     "", true);
  ParameterBase::register_element<const char*>  ("ap_passwd",              ap_passwd,              "",     "", true);
  ParameterBase::register_element<const char*>  ("network_master_addr",    network_master_addr,    "",     "", false);
  ParameterBase::register_element<uint16_t>     ("network_master_ws_port", network_master_ws_port, "",     8081, false);

  define_onchange_task(&handle_credentials_changed, ap_ssid);
  define_onchange_task(&handle_credentials_changed, ap_passwd);

#if MARVIN_WEBSOCKETS
  define_onchange_task(&handle_network_master_changed, network_master_addr);
  define_onchange_task(&handle_network_master_changed, network_master_ws_port);
#endif

  connection_establish_task_id = define_delayed_task(&connection_establish_task, 500, true, nullptr, false);
  try_reconnect_task_id = define_delayed_task(&try_reconnect_task, WIFI_RETRY_INTERVAL, true, nullptr, false);
  clean_monitored_messages_task_id = define_delayed_task(&clean_monitored_messages, 1000, true, nullptr, false);

#if OTA_ENABLED
  LOG_INFO(F("Bus configuration setting: Arduino OTA is enabled."));
#else
  LOG_INFO(F("Bus configuration setting: Arduino OTA is disabled."));
#endif

#if MARVIN_WEBSOCKETS
  LOG_INFO(F("Bus configuration setting: WebSocket communication is enabled."));
#else
  LOG_INFO(F("Bus configuration setting: WebSocket communication is disabled."));
#endif

#if MESSAGE_REDUNDANCY
  LOG_INFO(F("Bus configuration setting: Sending of redundant messages is enabled."));
#else
  LOG_INFO(F("Bus configuration setting: Sending of redundant messages is disabled."));
#endif

  return true;
}

void bus_recovery()
{
  WiFi.disconnect();
  WiFi.mode(WIFI_AP_STA);

  IPAddress ap_ip(192, 168, 2, 1);
  ILOG(F("Setting soft-AP configuration ... "));
  bool res = WiFi.softAPConfig(ap_ip, ap_ip, IPAddress(255, 255, 255, 0));
  LOG(res ? F("OK") : F("Failed!"));

  ILOG(F("Setting soft-AP ... "));
  res = WiFi.softAP(serial_number(), device_pin(), 1, false, 2);
  LOG(res ? F("OK") : F("Failed!"));

  ILOG(F("Soft-AP IP address = "));
  LOG(WiFi.softAPIP().toString());

  FLOG(F("AP started: %s - %s\r\n"), serial_number(), device_pin());

  if (server != nullptr)
  {
    delete server;
  }

  if (dns_server != nullptr)
  {
    delete dns_server;
  }

  server = new ESP8266WebServer(80);
  dns_server = new DNSServer();

  dns_server->setErrorReplyCode(DNSReplyCode::NoError);
  dns_server->start((byte)53, "*", ap_ip);

  server->on("/", [&]()
  {
    LOG(F("Sending configuration page."));
    server->sendHeader(F("Cache-Control"), F(" no-cache, no-store, must-revalidate"));
    server->sendHeader(F("Expires"), " 0");
    server->send(200, F("text/html"), wifi_htm);
  });

  server->on("/connect",  [&]()
  {
    server->send(200, F("text/html"), F("connecting..."));
    delay(25);

    // Setting the parameters ap_ssid and ap_passwd will force a connection to be established
    set<const char*>(ap_ssid, server->arg("s").c_str());
    set<const char*>(ap_passwd, server->arg("p").c_str());

    FLOG(F("Saved new credentials: '%s' - '%s'\r\n"),
      get<const char*>(ap_ssid),
      get<const char*>(ap_passwd));

    server->close();

  });

  server->begin();

  enable_task(try_reconnect_task_id);
  enable_task(connection_establish_task_id);

  define_delayed_task(
    [](task_id_t, void*) -> void
    {
      if (WiFi.status() != WL_CONNECTED)
      {
        server->handleClient();
      }
      else
      {
        server->stop();

        delete server;
        server = nullptr;
        delete dns_server;
        dns_server = nullptr;

        // We are connected to an AP, so we disable our own AP
        WiFi.mode(WIFI_STA);
        set<operational_status_t>(device_status, online);
        disable_task(try_reconnect_task_id);
        remove_scheduled_task();
      }
    }, 10, true);

}

bool establish_connection ()
{
  handle_credentials_changed(0, nullptr);
  return true;
}

class send_args
{
public:
  size_t counter;
  const char* msg_str;

  send_args() : counter(0), msg_str(nullptr)
  {}

  send_args(const char* str) : counter(0), msg_str(str)
  {}

  send_args(size_t count, const char* str) : counter(count), msg_str(str)
  {}

  ~send_args()
  {
    if (msg_str != nullptr)
    {
      delete msg_str;
      msg_str = nullptr;
    }
  }
};

IRAM_ATTR
Message parse_message(const char * packet)
{
  // @TODO Protect the system by guarding this with a try/catch
  Message message = Message(new DeviceMessage());
  deserializeJson(message->protected__get_doc(), packet);

  message->message_id = message->protected__get_doc()[json_message_id].as<uint16_t>();
  message->source_id = message->protected__get_doc()[json_source_id].as<uint8_t>();
  message->counter = message->protected__get_doc()[json_counter].as<uint8_t>();
  message->rtr = message->protected__get_doc()[json_rtr].as<uint8_t>() != 0;

  return message;
}

IRAM_ATTR
bool send (const Message message)
{
  if(!can_send(message->message_id))
  {
    return false;
  }

  message->protected__get_doc()[json_source_id] = message->source_id;
  message->protected__get_doc()[json_message_id] = message->message_id;
  message->protected__get_doc()[json_rtr] = (message->rtr ? 1 : 0);
  message->protected__get_doc()[json_counter] = message_counter;

  if (message_counter >= 255)
    message_counter = 0;
  else
    ++message_counter;

  bool ret = false;

  size_t buffer_size = measureJson(message->protected__get_doc()) + 1;
  char* serialized_json = new char[buffer_size];
  serializeJson(message->protected__get_doc(), serialized_json, buffer_size);

#if MARVIN_WEBSOCKETS
  if (ws_connected)
  {
    ret = ws_client.send(serialized_json, buffer_size - 1);
  }
#endif

#if MESSAGE_REDUNDANCY
  send_args* pargs = new send_args(2, serialized_json);

  udp_socket.beginPacket(broadcast_ip, local_port);
  udp_socket.write(serialized_json);
  ret |= 0 != udp_socket.endPacket();

  // Send each message three times in an interval of 100ms
  // Rationale: To ensure successful transmission of UPD packets,
  // messages should be sent three times in an intervall of 100ms

  define_delayed_task([](task_id_t task_id, void* pargs) -> void
    {
      send_args & args = *((send_args*)pargs);

      if (args.counter == 0)
      {
        delete &args;
        remove_task_by_id(task_id);
      }
      else
      {
        udp_socket.beginPacket(broadcast_ip, local_port);
        udp_socket.write(args.msg_str);
        udp_socket.endPacket();
        --args.counter;
      }
    }, 100, true, pargs);
#else
  udp_socket.beginPacket(broadcast_ip, local_port);
  //serializeJson(message->protected__get_doc(), udp_socket);
  udp_socket.write(serialized_json, buffer_size - 1);
  ret |= 0 != udp_socket.endPacket();
  delete serialized_json;
#endif

  return (ret);
}

IRAM_ATTR
void bus_receive_and_handle()
{
#if MARVIN_WEBSOCKETS
    // let the websockets client check for incoming messages
  if (ws_client.available())
  {
    ws_client.poll();
  }
#endif

  handle_ota();

  size_t packet_size = udp_socket.parsePacket();
  if (packet_size > 0)
  {
    udp_socket.read(packet_buffer, RX_PACKET_MAX_SIZE);
    packet_buffer[packet_size] = '\0';
    Message message = parse_message(packet_buffer);

    if (!is_recurrent_message(message))
    {
      handle(message);
    }
  }

  return;
}

}

#endif
