//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "service.h"
#include "device.h"
#include "device_message.h"

namespace marvin
{

enum iss_error_code_t
{
  device_id_unchanged = iss_error + 1
};

void handle_iss_service (const Message message)
{

  const char* destination_sn = message->get_payload<const char*>(json_dest_sn);
  if (0 != memcmp (destination_sn, serial_number(), 6))
  {
    return;
  }

  uint8_t stat = no_error;
  bool restart_needed = false;

  uint8_t id = get<uint8_t>(device_id);
  uint8_t new_id = message->get_payload<uint8_t>(json_new_id);

  if (id != 0 && id == new_id)
  {
    stat = device_id_unchanged;
  }
  else
  {
    restart_needed = true;

    // Set Device-ID
    set<uint8_t>(device_id, new_id);

    FLOG(F("New Device-ID '%d' stored.\r\n"), get<uint8_t>(device_id));
  }

  Message response = Message(new DeviceMessage(get<uint8_t>(device_id),
                                                            message->message_id + 1));
  response->set_payload(json_status, stat);
  send(response);

  if (restart_needed)
  {
    LOG(F("Device-ID was changed. Will need to restart."));
    restart();
  }
  else
  {
//    free_service(ISS, source_id);
  }
}

}
