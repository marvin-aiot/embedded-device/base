//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace marvin
{

class MessageSource
{
public:
  MessageSource() : message_id(0), source_id(0)
  {}

  MessageSource(const MessageSource& msg)
  : message_id(msg.message_id), source_id(msg.source_id)
  {}

  MessageSource(uint16_t message_id, uint8_t source_id)
  : message_id(message_id), source_id(source_id)
  {}

  MessageSource& operator=(const MessageSource& msg)
  {
    message_id = msg.message_id;
    source_id = msg.source_id;
    return *this;
  }

  MessageSource clone() const
  {
    return MessageSource(*this);
  }

  const_string to_string() const
  {
    String s("[");
    s += this->source_id;
    s += "|";
    s += this->message_id;
    s += "]";

    char* result = new char[s.length() + 1];
    strcpy(result, s.c_str());
    result[s.length()] = '\0';

    return std::move(const_string(result));
  }

  static MessageSource from_string(const char* s)
  {
    size_t len = strlen(s);
    char* str = new char[len + 1];
    memcpy(str, s, len);
    str[len] = '\0';

    char * pch;
    pch = strtok (&str[1], "|");
    uint8_t s_id = atoi(pch);
    pch = strtok (NULL, "]");
    uint16_t m_id = atoi(pch);

#if MARVIN_VERBOSITY_ERROR
    if (m_id == 0 || s_id == 0)
    {
        FLOG_ERROR(F("Reading MessageSource from '%s' failed.\r\n"), str);
    }
#endif

    delete str;

    return std::move(MessageSource(m_id, s_id));
  }

  uint16_t message_id;
  uint8_t source_id;
};

}
