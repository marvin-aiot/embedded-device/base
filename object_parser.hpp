//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <list>
#include <vector>

namespace marvin
{

template <typename T> struct is_container : std::false_type {};
template <typename T> struct is_container <std::vector<T>> : std::true_type {};
//template <typename T> struct is_container <const std::vector<T>> : std::true_type {};
template <typename T> struct is_container <std::list<T>> : std::true_type {};
//template <typename T> struct is_container <const std::list<T>> : std::true_type {};

template <typename T>
void liberate(T& obj, typename std::enable_if<!(std::is_same<T, char*>::value)>::type* = 0)
{
  (void) obj;
}

template <typename T>
void liberate(T& obj, typename std::enable_if<std::is_same<T, char*>::value>::type* = 0)
{
  if (obj)
  {
    delete obj;
  }
}

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<std::is_same<T, const char*>::value>::type* = 0)
{
  size_t len = strlen(obj);
  char* val = new char[len + 1];
  memcpy(val, obj, len);
  val[len] = '\0';

  return const_string(val);
}

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<(std::is_fundamental<T>::value && !std::is_same<T, bool>::value)>::type* = 0)
{
  String s(obj);
  size_t len = s.length();
  char* val = new char[len + 1];
  memcpy(val, s.c_str(), len);
  val[len] = '\0';
  return const_string(val);
}

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<std::is_class<T>::value && !is_container<T>::value>::type* = 0)
{
  return obj.to_string();
}

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<std::is_enum<T>::value>::type* = 0)
{
  return stringify<int>((int) obj);
}

const char true_str[] = "1";
const char false_str[] = "0";

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<std::is_same<T, bool>::value>::type* = 0)
{
  char* result = new char[2] {0};
  result[0] = (obj ? '1' : '0');
  return const_string(result);
}

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<std::is_same<T, String>::value>::type* = 0)
{
  size_t len = obj->length();
  char* val = new char[len + 1];
  memcpy(val, obj->c_str(), len);
  return const_string(val);
}

template <typename T>
const_string stringify(const T& obj, typename std::enable_if<is_container<T>::value>::type* = 0)
{
  std::list<const_string> elements;
  for (auto it = obj.cbegin(); it != obj.cend(); ++it)
  {
    elements.push_back(stringify<typename T::value_type>((typename T::value_type)*it));
  }

  uint32_t str_len = 2 + (2 * (elements.size() - 1)); // Start and end symbol and separators
  for (auto it = elements.cbegin(); it != elements.cend(); ++it)
  {
    str_len += strlen(it->get());
  }

  char* val = new char[str_len + 1];
  val[0] = '<';
  val[str_len - 1] = '>';
  val[str_len] = '\0';

  uint32_t str_pos = 1;
  for (auto it = elements.cbegin(); it != elements.cend(); ++it)
  {
    uint32_t len = strlen(it->get());
    memcpy(&val[str_pos], it->get(), len);
    str_pos += len;
    val[str_pos++] = '$';
    val[str_pos++] = '$';
  }

  // Container start is '<'
  // Container end is '>'
  // Element separator is '$$'

  return const_string(val);
}

template <typename T>
T read(const char* s, typename std::enable_if<std::is_class<T>::value && !is_container<T>::value>::type* = 0)
{
  return T::from_string(s);
}

template <typename T>
T read(const char* s, typename std::enable_if<(std::is_integral<T>::value && !std::is_same<T, bool>::value)>::type* = 0)
{
  return atol(s);
}

template <typename T>
T read(const char* s, typename std::enable_if<std::is_same<T, bool>::value>::type* = 0)
{
  return atol(s) != 0;
}

template <typename T>
T read(const char* s, typename std::enable_if<std::is_enum<T>::value>::type* = 0)
{
  return (T) read<int>(s);
}

template <typename T>
T read(const char* s, typename std::enable_if<std::is_same<T, const char*>::value>::type* = 0)
{
  size_t len = strlen(s);
  char* val = new char[len + 1];
  memcpy(val, s, len);
  val[len] = '\0';
  return val;
}

template <typename T>
T read(const char* s, typename std::enable_if<std::is_same<T, const_string>::value>::type* = 0)
{
  size_t len = strlen(s);
  const char* val = new char[len + 1];
  memcpy(val, s, len);
  val[len] = '\0';
  return std::move(const_string(val));
}

template <typename T>
T read(const char* s, typename std::enable_if<std::is_same<T, string>::value>::type* = 0)
{
  size_t len = strlen(s);
  const char* val = new char[len + 1];
  memcpy(val, s, len);
  val[len] = '\0';
  return std::move(string(val));
}

template <typename T>
T read(const char* s, typename std::enable_if<is_container<T>::value>::type* = 0)
{
  T container = T();
  const char* end = &s[strlen(s) - 1];
  const char* prev = s + 1;
  const char* next = strstr(prev, "$$");

  while (next)
  {
    char * elm = new char[next - prev + 2];
    memcpy(elm, prev, next - prev + 1);
    elm[next - prev + 1] = '\0';
    container.push_back(read<typename T::value_type>((const char*)&elm[0]));
    delete elm;
    prev = next + 2;
    next = strstr(prev, "$$");
  }

  char * elm = new char[end - prev + 1];
  memcpy(elm, prev, end - prev);
  elm[end - prev + 1] = '\0';
  container.push_back(read<typename T::value_type>((const char*)&elm[0]));
  delete elm;

  return container;
}

template <typename T>
T cloner(const T& obj, void*);

template <typename T>
static T cloner(const T& obj, typename std::enable_if<std::is_class<T>::value && !is_container<T>::value>::type* = 0)
{
  return obj.clone();
}

template <typename T>
static T cloner(const T& obj, typename std::enable_if<std::is_fundamental<T>::value>::type* = 0)
{
  return obj;
}

template <typename T>
static T cloner(const T& obj, typename std::enable_if<std::is_enum<T>::value>::type* = 0)
{
  return obj;
}

template <typename T>
T cloner(T& obj, typename std::enable_if<std::is_same<T, string>::value>::type* = 0)
{
  return obj;
}

template <typename T>
T cloner(T& obj, typename std::enable_if<std::is_same<T, const_string>::value>::type* = 0)
{
  return obj;
}

template <typename T>
T cloner(const T& obj, typename std::enable_if<std::is_same<T, const char*>::value>::type* = 0)
{
  size_t len = strlen(obj);
  char* val = new char[len + 1];
  strncpy(val, obj, len);
  val[len] = '\0';
  return val;
}

template <typename T>
T cloner(const T& obj, typename std::enable_if<std::is_same<T, char*>::value>::type* = 0)
{
  size_t len = strlen(obj);
  char* val = new char[len + 1];
  memcpy(val, obj, len);
  val[len] = '\0';
  return val;
}

template <typename T>
static T cloner(const T& obj, typename std::enable_if<is_container<T>::value>::type* = 0)
{
  return T(obj);
}

}
