
#include "Arduino.h"
#include "globals.h"

namespace marvin
{

const char* json_ack            PROGMEM = "ACK";
const char* json_argument       PROGMEM = "ARGUMENT";
const char* json_command        PROGMEM = "COMMAND";
const char* json_counter        PROGMEM = "COUNTER";
const char* json_data           PROGMEM = "DATA";
const char* json_data_crc       PROGMEM = "DATA_CRC";
const char* json_data_size      PROGMEM = "DATA_SIZE";
const char* json_dest_id        PROGMEM = "DESTINATION_ID";
const char* json_dest_sn        PROGMEM = "DESTINATION_SN";
const char* json_error_code     PROGMEM = "ERROR_CODE";
const char* json_expect_more    PROGMEM = "EXPECT_MORE";
const char* json_frame_no       PROGMEM = "FRAME_NO";
const char* json_ip_address     PROGMEM = "IP_ADDRESS";
const char* json_mac_address    PROGMEM = "MAC_ADDRESS";
const char* json_md5            PROGMEM = "MD5";
const char* json_message_id     PROGMEM = "MESSAGE_ID";
const char* json_messages       PROGMEM = "MESSAGES";
const char* json_new_id         PROGMEM = "NEW_ID";
const char* json_offset         PROGMEM = "OFFSET";
const char* json_parameter      PROGMEM = "PARAMETER";
const char* json_parameters     PROGMEM = "PARAMETERS";
const char* json_payload        PROGMEM = "PAYLOAD";
const char* json_occupied       PROGMEM = "OCCUPIED";
const char* json_read_data      PROGMEM = "READ_DATA";
const char* json_rtr            PROGMEM = "RTR";
const char* json_serial_number  PROGMEM = "SERIAL_NUMBER";
const char* json_status         PROGMEM = "STATUS";
const char* json_source_id      PROGMEM = "SOURCE_ID";

const char* parameter_not_found_str PROGMEM = "PARAMETER_NOT_FOUND";

static bool interrupts_enabled_flag = false;

IRAM_ATTR
bool interrupts_enabled()
{
  return interrupts_enabled_flag;
}

void enable_interrupts()
{
  interrupts_enabled_flag = true;
}

void disable_interrupts()
{
  interrupts_enabled_flag = false;
}

}
