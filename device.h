//// Copyright (C) 2020 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once


#define VERSION_MAJOR "0"
#define VERSION_MINOR "21"


namespace marvin
{

bool initialize();
void on_run_level_changed(run_level_t run_level);
void step();


enum device_parameters
{
  restart_counter = 100,
  hb_send_interval
};

extern const char* m_version;
extern const char* m_build_datetime;

extern time_t m_now;
extern uint32_t m_now_ms;

void initialize_device();

void recover();

void restart();

inline
const char* version ()
{
  return m_version;
}

inline
const char* build_datetime ()
{
  return m_build_datetime;
}

// @TODO Make serial number a uint32_t
inline
const char* serial_number()
{
  return serial_number_str;
}

inline
const char* device_pin ()
{
  return device_pin_str;
}

inline
time_t now()
{
  return m_now;
}

inline
uint32_t now_ms()
{
  return m_now_ms;
}

}
