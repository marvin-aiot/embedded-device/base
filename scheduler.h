
#pragma once

#include "Arduino.h"

namespace marvin
{
/*

Register a function 'void(*)()' to be executed:
 - in x milliseconds
 - every x miliseconds
 - a condition is met function 'bool(*)()'
 - Parameter or Variable is modified

 */

typedef uint32_t task_id_t;
typedef uint32_t delay_t;
typedef std::function<void(task_id_t, void*)> task_fn_t;
typedef std::function<bool(void*)> condition_fn_t;

extern task_id_t current_task_id;

bool reschedule_task_by_id(task_id_t id, uint32_t scheduled_time);
bool remove_task_by_id(task_id_t id);

task_id_t define_scheduled_task(task_fn_t task_fn, uint32_t scheduled_time, void* args = NULL, bool enabled = true);

task_id_t define_delayed_task(task_fn_t task_fn, delay_t delay, bool reschedule, void* args = NULL, bool enabled = true);

task_id_t define_onchange_task(task_fn_t task_fn, uint16_t element, void* args = NULL, bool enabled = true);

task_id_t define_oncondition_task(task_fn_t task_fn, condition_fn_t condition_fn, void* args = NULL, bool enabled = true);

bool delete_task(task_id_t task_id);
bool enable_task(task_id_t task_id);
bool disable_task(task_id_t task_id);
bool task_enabled(task_id_t task_id);

void run_scheduled_tasks(uint32_t now);

#define reschedule_task(scheduled_time)                               \
reschedule_task_by_id(current_task_id, scheduled_time)

#define remove_scheduled_task()                                       \
remove_task_by_id(current_task_id)

#define timer_id_from_parameter(parameter)                            \
parameter ## _onchange_task_id

#define timer_id_from_timer_name(timer_name)                          \
timer_name ## _delayed_task_id

#define timer_id_from_condition_name(condition_name)                  \
condition_name ## _condition_task_id

#define enable_onchange_task(parameter)                               \
enable_task(timer_id_from_parameter(parameter))

#define disable_onchange_task(parameter)                              \
disable_task(timer_id_from_parameter(parameter))

#define enable_delayed_timer(timer_name)                              \
enable_task(timer_id_from_timer_name(timer_name))

#define disable_delayed_timer(timer_name)                             \
disable_task(timer_id_from_timer_name(timer_name))

#define create_onchange_task(parameter, body)                         \
task_id_t timer_id_from_parameter(parameter) = 0;                     \
IRAM_ATTR                                                             \
void parameter ## _onchange_timer(task_id_t, void*)                   \
{                                                                     \
  body                                                                \
}

#define initialize_onchange_task(parameter)                           \
  []()                                                                \
  {                                                                   \
    timer_id_from_parameter(parameter) =                              \
      define_onchange_task(&parameter ## _onchange_timer,             \
        parameter, NULL, false);                                      \
  }()

#define create_delayed_timer(timer_name, body)                        \
task_id_t timer_id_from_timer_name(timer_name) = 0;                   \
IRAM_ATTR                                                             \
void timer_name ## _delayed_timer(task_id_t, void*)                   \
{                                                                     \
  body                                                                \
}

#define initialize_delayed_timer(timer_name, delay)                   \
  []()                                                                \
  {                                                                   \
    timer_id_from_timer_name(timer_name) =                            \
      define_delayed_task(&timer_name ## _delayed_timer,              \
        delay, true, nullptr, false);                                 \
  }()
  
#define create_conditional_task(condition_name, condition, body)      \
task_id_t timer_id_from_condition_name(condition_name) = 0;           \
IRAM_ATTR                                                             \
bool condition_name ## _condition_fn(void*)                           \
{                                                                     \
  condition                                                           \
}                                                                     \
IRAM_ATTR                                                             \
void condition_name ## _condition_body(task_id_t, void*)              \
{                                                                     \
  body                                                                \
}

#define initialize_conditional_task(condition_name)                   \
  []()                                                                \
  {                                                                   \
    timer_id_from_condition_name(condition_name) =                    \
      define_oncondition_task(&condition_name ## _condition_body,     \
        &condition_name ## _condition_fn, NULL, false);               \
  }()
}
