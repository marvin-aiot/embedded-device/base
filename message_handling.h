//// Copyright (C) 2020 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "device_message.h"
#include "parameter.hpp"
#include "message_source.hpp"
#include "bus.h"
#include "scheduler.h"

#include <forward_list>

namespace marvin
{


struct message_generator
{
  uint16_t message_id;
  bool (*generator_func)(task_id_t, void*);
};

extern std::forward_list<message_generator> m_message_generators;

bool send(uint16_t msg_id);

inline
void push_message_generator(uint16_t msg_id, bool (*generator)(task_id_t, void*))
{
  // @TODO Check if msg_id is already registered
  m_message_generators.push_front(message_generator {msg_id, generator});
}

void handle(Message message);

void push_message_source_internal(uint16_t msg_src, std::function<void (const Message)> fun);
void call_on_relevant_messages (std::function<void (uint8_t dev_id, uint16_t msg_ig)> fn);

template <typename T>
inline
// @REVIEW: Consider msg_src would be a list of MessageSource objects.
void push_message_source(uint16_t msg_src, uint16_t container, T (*parser)(const Message))
{
  push_message_source_internal(msg_src, [container, parser](const Message data)
      {
        set<T>(container, parser(data));
      });
}

#define enable_message_generator(msg_id)                                                            \
  enable_task(* msg_ ## msg_id ## _task_id)

#define disable_message_generator(msg_id)                                                           \
  disable_task(* msg_ ## msg_id ## _task_id)

#define define_message_generator(msg_id, generator)                                                 \
  uint32_t * msg_ ## msg_id ## _task_id = new uint32_t(0);                                          \
  IRAM_ATTR                                                                                         \
  bool msg_ ## msg_id ## _generator(task_id_t task_id, void* arg)                                   \
  {                                                                                                 \
    (void) task_id;                                                                                 \
    (void) arg;                                                                                     \
    bool success = false;                                                                           \
    generator                                                                                       \
    return success;                                                                                 \
  }

#define register_message_generator(msg_id, interval_parameter)                                      \
  []()                                                                                              \
  {                                                                                                 \
    push_message_generator(msg_id, & msg_ ## msg_id ## _generator);                                 \
    * msg_ ## msg_id ## _task_id = define_delayed_task([](task_id_t task_id, void* arg) -> void     \
    {                                                                                               \
      msg_ ## msg_id ## _generator(task_id, arg);                                                   \
    }, interval_parameter, true, NULL, false);                                                      \
    return msg_ ## msg_id ## _task_id != NULL ? * msg_ ## msg_id ## _task_id : 0;                   \
  }()

}
