//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "ArduinoJson.h"
#include <memory>

namespace marvin
{
class DeviceMessage;

typedef std::shared_ptr<DeviceMessage> Message;

class DeviceMessage
{
public:
  DeviceMessage(uint8_t source_id, uint16_t msg_id, bool rtr = false)
  : source_id(source_id), message_id(msg_id), rtr(rtr), doc(DynamicJsonDocument(2048))
  {
  }

  DeviceMessage()
  : doc(DynamicJsonDocument(2048))
  {}

  DeviceMessage(DeviceMessage&& that)
  : source_id(that.source_id), message_id(that.message_id), counter(that.counter), rtr(that.rtr), doc(that.doc)
  {
  }

  template<typename T>
  inline
  void set_payload(const char* key, T value)
  {
    this->doc[key] = value;
  }

  template<typename T>
  inline
  T get_payload(const char* key)
  {
    return this->doc[key];
  }

  inline
  DynamicJsonDocument& protected__get_doc()
  {
    return doc;
  }

  ~DeviceMessage() {}

  uint8_t source_id;
  uint16_t message_id;
  uint8_t counter;
  bool rtr;

private:
  mutable DynamicJsonDocument doc;
};

}
