
#include "scheduler.h"
#include "globals.h"
#include "parameter.hpp"

#include <forward_list>
#include <list>

namespace marvin
{
  struct element_changed_task_t
  {
    task_id_t id;
    task_fn_t func;
    uint16_t parameter_id;
    bool enabled;
    void* argument;
  };

  struct conditional_task_t
  {
    task_id_t id;
    task_fn_t func;
    condition_fn_t condition;
    bool enabled;
    void* argument;
  };

  struct delayed_task_t
  {
    task_id_t id;
    task_fn_t func;
    uint32_t scheduled_time;
    delay_t delay;
    bool reschedule;
    bool enabled;
    void* argument;
  };

  struct reschedule_element_t
  {
    delayed_task_t* ptask;
    uint32_t new_scheduled_time;
  };

  static std::forward_list<element_changed_task_t*> element_changed_tasks = {};
  static std::forward_list<conditional_task_t*> conditional_tasks = {};

  /* scheduled_tasks is a sorted list of tasks to be executed. Sorting follows the delay parameter. */
  static std::list<delayed_task_t*> scheduled_tasks = {};

  /* delay_tolerance defines how long into the future tasks might be executed.
   *  It's usually better to execute a task a few milliseconds earlier than many later. */
  static const delay_t delay_tolerance = 5;

  task_id_t current_task_id = 0;
  std::forward_list<reschedule_element_t> tasks_to_reschedule = {};
  std::forward_list<delayed_task_t*> tasks_to_remove = {};

  static task_id_t next_task_id_val = 0;

  inline
  task_id_t next_task_id()
  {
    // @REVIEW How to handle task id overflow? There will be a clash of ids!
    //         Idea: have a list containing all ids in use. push new one here, remove when deleted. Then find the smalles uint not in the list.
    return ++next_task_id_val;
  }

  IRAM_ATTR
  void insert_sorted_scheduled(delayed_task_t* ptask)
  {
    if (scheduled_tasks.empty())
    {
      scheduled_tasks.push_front(ptask);
    }
    else
    {
      uint32_t scheduled_time = ptask->scheduled_time;
      auto end_it = scheduled_tasks.end();
      auto first_it = scheduled_tasks.begin();
      auto last_it = end_it;
      --last_it;

      // If overflow_detected, skip elements while scheduled_time of list element > scheduled_time of *ptask
      if ((*last_it)->scheduled_time < (*first_it)->scheduled_time)
      {
        while (first_it != end_it && scheduled_time > (*first_it)->scheduled_time)
        {
          //FLOG(F("* %d\r\n"),  (*first_it)->scheduled_time);
          ++first_it;
        }
      }

      // Skip elements while scheduled_time of list element < scheduled_time of *ptask
      while (first_it != end_it && (*first_it)->scheduled_time < scheduled_time)
      {
        //FLOG(F("+ %d\r\n"),  (*first_it)->scheduled_time);
        ++first_it;
      }

      scheduled_tasks.insert(first_it, ptask);
    }

    /*
    LOG(F("================================ BEGIN ===========================================\r\n\r\n"));

    auto end_it2 = scheduled_tasks.end();
    auto first_it2 = scheduled_tasks.begin();

    while (first_it2 != end_it2)
    {
      FLOG(F("* %d\r\n"),  (*first_it2)->scheduled_time);
      ++first_it2;
    }

    FLOG(F("\r\nInserted task with id %ld for scheluled time %ld\r\n"), ptask->id, ptask->scheduled_time);
    LOG(F("================================ END ===========================================\r\n\r\n"));
    */
  }

  inline
  bool compare_schedules (const delayed_task_t* l, const delayed_task_t* r)
  {
    return l->scheduled_time < r->scheduled_time;
  }

  IRAM_ATTR
  bool reschedule_task_by_id(task_id_t id, uint32_t scheduled_time)
  {
    for (delayed_task_t * ptask : scheduled_tasks)
    {
      if (id == ptask->id)
      {
        reschedule_element_t elm {ptask, scheduled_time};
        tasks_to_reschedule.push_front(elm);

// @REVIEW For some reason (to analyse) this breaks all hell (sometimes)
//        scheduled_tasks.remove(ptask);
//        ptask->scheduled_time = scheduled_time;
//        insert_sorted_scheduled(ptask);

        return true;
      }
    }

    return false;
  }

  bool remove_task_by_id(task_id_t id)
  {
    for (delayed_task_t * ptask : scheduled_tasks)
    {
      if (id == ptask->id)
      {
        tasks_to_remove.push_front(ptask);
        return true;
      }
    }

    return false;
  }

  IRAM_ATTR
  bool set_task_enabled_flag(task_id_t task_id, bool enabled)
  {
    for (delayed_task_t * ptask : scheduled_tasks) // @REVIEW This generates a warning: 'ptask' is used uninitialized
    {
      if (ptask->id == task_id)
      {
        ptask->enabled = enabled;
        return true;
      }
    }

    for (element_changed_task_t * ptask : element_changed_tasks)
    {
      if (ptask->id == task_id)
      {
        ptask->enabled = enabled;
        return true;
      }
    }

    for (conditional_task_t * ptask : conditional_tasks)
    {
      if (ptask->id == task_id)
      {
        ptask->enabled = enabled;
        return true;
      }
    }

    FLOG(F("Could not set the enabled flag of task %d to %s\r\n\r\n"), task_id, enabled ? "true" : "false");
    return false;
  }

  bool enable_task(task_id_t task_id)
  {
    return set_task_enabled_flag(task_id, true);
  }

  bool disable_task(task_id_t task_id)
  {
    return set_task_enabled_flag(task_id, false);
  }

  bool task_enabled(task_id_t task_id)
  {
    for (delayed_task_t * ptask : scheduled_tasks) // @REVIEW This generates a warning: 'ptask' is used uninitialized
    {
      if (ptask->id == task_id)
      {
        return ptask->enabled;
      }
    }
    for (element_changed_task_t * ptask : element_changed_tasks)
    {
      if (ptask->id == task_id)
      {
        return ptask->enabled;
      }
    }

    for (conditional_task_t * ptask : conditional_tasks)
    {
      if (ptask->id == task_id)
      {
        return ptask->enabled;
      }
    }

    FLOG(F("Could not get the enabled flag of unknown task %d\r\n\r\n"), task_id);
    return false;
  }

  task_id_t define_scheduled_task(task_fn_t task_fn, uint32_t scheduled_time, void* args, bool enabled)
  {
    task_id_t id = next_task_id();
    insert_sorted_scheduled(new delayed_task_t {id, task_fn, scheduled_time, 0, false, enabled, args});
    return id;
  }

  task_id_t define_delayed_task(task_fn_t task_fn, delay_t delay, bool reschedule, void* args, bool enabled)
  {
    task_id_t id = next_task_id();
    insert_sorted_scheduled(new delayed_task_t {id, task_fn, millis() + delay, delay, reschedule, enabled, args});
    return id;
  }

  task_id_t define_onchange_task(task_fn_t task_fn, uint16_t element, void* args, bool enabled)
  {
    task_id_t id = next_task_id();
    element_changed_tasks.push_front(new element_changed_task_t {id, task_fn, element, enabled, args});
    return id;
  }

  task_id_t define_oncondition_task(task_fn_t task_fn, condition_fn_t condition_fn, void* args, bool enabled)
  {
    task_id_t id = next_task_id();
    conditional_tasks.push_front(new conditional_task_t {id, task_fn, condition_fn, enabled, args});
    return id;
  }

  bool delete_task(uint32_t task_id)
  {
    bool found = false;

    do
    {
      for (delayed_task_t * ptask : scheduled_tasks) // @REVIEW This generates a warning: 'ptask' is used uninitialized
      {
        if (ptask->id == task_id)
        {
          scheduled_tasks.remove(ptask); // Warning: This invalidates the interator in the loop! Do not continue the loop after this.
          found = true;
          break;
        }
      }

      if (found)
      {
        break;
      }

      for (element_changed_task_t * ptask : element_changed_tasks)
      {
        if (ptask->id == task_id)
        {
          element_changed_tasks.remove(ptask); // Warning: This invalidates the interator in the loop! Do not continue the loop after this.
          found = true;
          break;
        }
      }

      if (found)
      {
        break;
      }

      for (conditional_task_t * ptask : conditional_tasks)
      {
        if (ptask->id == task_id)
        {
          conditional_tasks.remove(ptask); // Warning: This invalidates the interator in the loop! Do not continue the loop after this.
          found = true;
          break;
        }
      }

      if (found)
      {
        break;
      }

    } while (false);

    return found;
  }

  // @REVIEW Could return the scheduled time of the next (future) task. So the device could sleep until then.
  IRAM_ATTR
  void run_scheduled_tasks(uint32_t now)
  {
    static uint32_t last_execution = 0;
    tasks_to_reschedule.clear();
    tasks_to_remove.clear();

    bool overflow_detected = now < last_execution;

    if (overflow_detected)
    {
      LOG(F("------------------- Timer overflow -------------------"));
    }

    //FLOG(F("\r\n+++ Running scheduled tasks. Now is %ld\r\n"), now);
    // Run all scheduled_tasks
    auto end_it = scheduled_tasks.end();
    auto first_it = scheduled_tasks.begin();

    while (first_it != end_it)
    {
      delayed_task_t & task = **first_it;
      uint32_t scheduled_time = task.scheduled_time;

      // @TODO Detect whether the task lies in the future, considering potential overflow of the time counter
      if ((overflow_detected && last_execution < scheduled_time) ||
         (!overflow_detected && ((scheduled_time <= now) ||
                                 (scheduled_time - now < delay_tolerance))))
      {
        //FLOG(F("++++ Scheduled Task executed. Requested for %ld, now is %ld, delta %ld.\r\n"), scheduled_time, now, now - scheduled_time);

        current_task_id = task.id;

        if (task.enabled)
        {
          task.func(task.id, task.argument);
        }

        if (task.reschedule)
        {
          // Reschedule the task, unless it's to be removed
          if (tasks_to_remove.empty() || current_task_id != tasks_to_remove.front()->id)
          {
            // Update the scheduled_time of the task and put it in a new position of the list
            task.scheduled_time += task.delay;

            delayed_task_t * ptask = *first_it;
            first_it = scheduled_tasks.erase(first_it);
            insert_sorted_scheduled(ptask);
          }
          else
          {
            ++first_it;
          }
        }
        else
        {
          // Remove task from the list, unless it's to be rescheduled
          if (tasks_to_reschedule.empty() || current_task_id != tasks_to_reschedule.front().ptask->id)
          {
            first_it = scheduled_tasks.erase(first_it);
          }
          else
          {
            ++first_it;
          }
        }
      }
      else
      {
        break;
      }

      yield(); // Give the background tasks some time to work
    }

    // Run all element_changed_tasks
    for (element_changed_task_t * ptask : element_changed_tasks)
    {
      element_changed_task_t & task = *ptask;
      if(task.enabled && ParameterBase::is_modified(task.parameter_id))
      {
        current_task_id = task.id;
        task.func(task.id, task.argument);
        yield(); // Give the background tasks some time to work
      }
    }

    // Run all conditional_tasks
    for (conditional_task_t * ptask : conditional_tasks)
    {
      conditional_task_t & task = *ptask;

      if (task.enabled && task.condition(task.argument))
      {
        current_task_id = task.id;
        task.func(task.id, task.argument);
        yield(); // Give the background tasks some time to work
      }
    }

    for (delayed_task_t * ptask : tasks_to_remove)
    {
      scheduled_tasks.remove(ptask);
      delete ptask;
    }

    for (reschedule_element_t & element : tasks_to_reschedule)
    {
      scheduled_tasks.remove(element.ptask);
      element.ptask->scheduled_time = element.new_scheduled_time;
      insert_sorted_scheduled(element.ptask);
    }

    current_task_id = 0;
    last_execution = now;
  }
}
