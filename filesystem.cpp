


#include "filesystem.h"
#include <memory>



namespace marvin
{

#ifndef ESP8266
const char* File::read_until (char terminal)
{
  return nullptr;
}

const char* File::read_line ()
{
  return nullptr;
}

size_t File::write (const char* data)
{
  return 0;
}

bool File::close ()
{
  return false;
}

bool FileSystem::begin()
{
  return false;
}

bool FileSystem::format()
{
  return false;

File FileSystem::open (char* path, char* mode)
{
  File f;
  return std::move(f);
}

File FileSystem::open (const StringSumHelper& path, char* mode)
{
  File f;
  return std::move(f);
}

bool FileSystem::exists (const StringSumHelper& path)
{
  return false;
}

bool FileSystem::exists (char* path)
{
  return false;
}


bool FileSystem::remove (char* path)
{
  return false;
}

bool FileSystem::remove (const StringSumHelper& path)
{
  return false;
}

FileSystem MARVINFS = FileSystem();

#endif

}
