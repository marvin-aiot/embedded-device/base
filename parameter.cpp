//// Copyright (C) 2019 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "parameter.hpp"
#include "Arduino.h"
#include "globals.h"
#include "filesystem.h"

#include "object_parser.hpp"

namespace marvin
{

std::forward_list<ParameterBase*> ParameterBase::m_parameter_table = {};

void ParameterBase::load()
{
  File file = MARVINFS.open(String("/") + this->name, "r");
  if (!file)
  {
    FLOG("Unable to load parameter: %s\r\n" ,this->name);
    return;
  }

  size_t len = file.size();
  char* s = new char[len + 1];
  file.readBytes(s, len);
  file.close();

  s[len] = '\0';

  // Store parameter in memory for quick access
  from_string(s, false);
}

void ParameterBase::store()
{
  File file = MARVINFS.open(String("/") + this->name, "w");
  if (!file)
  {
    FLOG("Unable to store parameter: %s\r\n", this->name);
    return;
  }

  file.print(to_string().get());
  file.close();
}


void ParameterBase::initialize()
{
  if (!persistent)
  {
    return;
  }

  if (MARVINFS.exists(String("/") + this->name))
  {
    load();
  }
  else
  {
    store();
  }
}

IRAM_ATTR
ParameterBase* ParameterBase::find_parameter(uint16_t param)
{
  for (auto it = m_parameter_table.cbegin(); it != m_parameter_table.cend(); ++it)
  {
    if ((*it)->id == param)
    {
      return *it;
    }
  }

  FLOG(F("ERROR: Parameter %d is not registered.\r\n"), param);
  return nullptr;
}

ParameterBase* ParameterBase::find_parameter_by_name(const char* param)
{
  for (auto it = m_parameter_table.cbegin(); it != m_parameter_table.cend(); ++it)
  {
    if (strcmp(param, (*it)->name) == 0)
    {
      return *it;
    }
  }

  FLOG(F("ERROR: Parameter '%s' is not registered.\r\n"), param);
  return nullptr;
}

IRAM_ATTR
bool ParameterBase::is_modified(uint16_t param)
{
  ParameterBase* p = find_parameter(param);
  return p == nullptr ? false : p->modified;
}

IRAM_ATTR
void ParameterBase::reset_modified(uint16_t param)
{
  ParameterBase* p = find_parameter(param);
  if (p != nullptr)
  {
    p->modified = false;
  }
}

void ParameterBase::dump_parameters()
{
  LOG_INFO(F("\r\n\r\n==================================================\r\n|                CONFIGURATION                   |\r\n=================================================="));
  for (auto it = m_parameter_table.cbegin(); it != m_parameter_table.cend(); ++it)
  {
    FLOG_INFO(F("|%-25s (%04d): %14s|\r\n"), (*it)->name, (*it)->id, (*it)->to_string());
  }
  LOG_INFO(F("=================================================="));
}

}


