
#include "Arduino.h"
#include "globals.h"

#include "message_handling.h"

#include "message_source.hpp"
#include "service.h"
#include "device.h"

namespace marvin
{

struct message_parser
{
  uint8_t source_device_id;
  uint16_t message_id;
  std::function<void (const Message data)> parser_func;
};

std::forward_list<message_parser> m_message_parsers = {};
std::forward_list<message_generator> m_message_generators = {};


void handle_dms_service (const Message message);
void handle_iss_service (const Message message);
void handle_pis_service (const Message message);
void handle_dus_service (const Message message);

void push_message_source_internal(uint16_t msg_src, std::function<void (const Message)> fun)
{
  const MessageSource& source = get<MessageSource>(msg_src);

  // @TODO Check if message_id and source_id is already registered

  m_message_parsers.push_front(message_parser {source.source_id, source.message_id, fun});
}

IRAM_ATTR
bool send(uint16_t msg_id)
{
  for (auto it = m_message_generators.cbegin(); it != m_message_generators.cend(); ++it)
  {
    if (msg_id == it->message_id)
    {
      return it->generator_func(0, NULL); // There can only be one registered generator per msg_id
    }
  }

  return false;
}

IRAM_ATTR
void call_on_relevant_messages (std::function<void (uint8_t dev_id, uint16_t msg_ig)> fn)
{
  for (auto it = m_message_parsers.cbegin(); it != m_message_parsers.cend(); ++it)
  {
    fn(it->source_device_id, it->message_id);
  }
}

inline
void handle_event(const Message message)
{
  for (auto it = m_message_parsers.cbegin(); it != m_message_parsers.cend(); ++it)
  {
    if (message->message_id == it->message_id && message->source_id == it->source_device_id)
    {
      it->parser_func(message);
      break; // There can only be one registered combination of message_id and source_id
    }
  }

  return;
}

inline
void handle_event_request(const Message message)
{
  if (!send(message->message_id))
  {
    FLOG_WARN(F("No handler for '%d' RTR known."), message->message_id);
  }

  return;
}

IRAM_ATTR
void handle(Message message)
{
  if (message->message_id >= 1024)
  {
    FLOG_ERROR(F("Invalid Message ID: %d\r\n"), message->message_id);
    return;
  }

  if ((message->message_id >= 128 && message->message_id < 200) || (message->message_id >= 960 && message->message_id < 1024))
  {
    FLOG_DEBUG(F("Handling request %d.\r\n"), message->message_id);

    switch (message->message_id)
    {
    case DMS:
      {
        handle_dms_service(message);
        break;
      }
    case ISS:
      {
        handle_iss_service(message);
        break;
      }
    case PIS:
      {
        handle_pis_service(message);
        break;
      }
    case DUS:
      {
        handle_dus_service(message);
        break;
      }
    default:
      {
        FLOG_ERROR(F("Invalid Service: %d\r\n"), message->message_id);
        break;
      }
    }
  }
  else
  {
    if (0 == message->rtr)
    {
      handle_event(message);
    }
    else if (message->source_id == 0 || message->source_id == get<uint8_t>(device_id))
    {
      handle_event_request(message);
    }
  }

  return;
}

}


#include "dms_service.h"
#include "iss_service.h"
#include "pis_service.h"
#include "dus_service.h"
