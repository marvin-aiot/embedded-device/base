
//#define CAN_BUS

//#ifdef CAN_BUS

#ifndef ESP8266

#include "Arduino.h"
#include <globals.h>
#include <marvin_device.h>

namespace marvin
{

bool bus_setup()
{
  return false;
}

void bus_recovery()
{
}

bool establish_connection ()
{
  return false;
}

bool send (const Message message)
{
  return false;
}

void bus_receive_and_handle ()
{
}
}

#endif
