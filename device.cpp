
#include "Arduino.h"
#include "globals.h"

#include "device.h"
#include "filesystem.h"
#include "device_message.h"
#include "message_handling.h"
#include "scheduler.h"
#include "bus.h"

#include <EEPROM.h>

#ifdef ESP8266
extern "C" {
#include "user_interface.h"
}
#endif

namespace marvin
{
  
const char* m_version = VERSION_MAJOR "." VERSION_MINOR;
const char* m_build_datetime = __DATE__ " " __TIME__;

time_t m_now = 0;
uint32_t m_now_ms = 0;
const char serial_number_str[17] = {0};
uint64_t serial_number_int; // @Note Serial Number is actually 7 bytes
const char device_pin_str[9] = {0};
uint32_t device_pin_int;

#if MARVIN_VERBOSITY_TRACE
  uint32_t step_counter = 0;
  uint16_t step_acc = 0;
  uint32_t step_longest = 0;
  uint32_t last_step = 0;
#endif

void restart()
{

  LOG(F("Preparing to reboot..."));
  delay(1000);
  
  // @TODO Could we switch to run_level RL0 before restarting?
  set<run_level_t>(device_run_level, RL0);
  
  delay(1000);
  LOG(F("Rebooting now!"));
  
    
  #ifdef ESP8266
    ESP.restart();
  #endif
}

define_message_generator(208,
{
  Message msg = Message(new DeviceMessage(get<uint8_t>(device_id), 208));
  msg->set_payload(json_status, get<operational_status_t>(device_status));
  msg->set_payload(json_offset, get<uint32_t>(hb_send_interval));
  success = send(msg);
});


bool send_device_identification (task_id_t, void*)
{
  // Send a Boot Complete Event
  Message msg = Message(new DeviceMessage(get<uint8_t>(device_id), 252));
  msg->set_payload(json_status, get<operational_status_t>(device_status));
  msg->set_payload(json_serial_number, String(serial_number()));
  return send(msg);
}

void handle_state_change(task_id_t task_id, void* args)
{
  (void) task_id;
  (void) args;

  ILOG_INFO(F("Device status: "));
  switch (get<operational_status_t>(device_status, true))
  {
    case power_up:
    {
      LOG_INFO(F("power_up"));
      set_iff<run_level_t>(device_run_level, RL0);
      break;
    }
    case operational:
    {
      LOG_INFO(F("operational"));
      set_iff<run_level_t>(device_run_level, RL2);
      break;
    }
    case maintenance:
    {
      LOG_INFO(F("maintenance"));
      set_iff<run_level_t>(device_run_level, RL1);
      break;
    }
    case recovery:
    {
      LOG_INFO(F("recovery"));
      set_iff<run_level_t>(device_run_level, RL1);
      bus_recovery();
      recover();
      break;
    }
    case online:
    {
      LOG_INFO(F("online"));
      set<operational_status_t>(device_status, get<uint8_t>(device_id) == 0 ? uncommissioned : operational);
      set_iff<run_level_t>(device_run_level, RL1);
      break;
    }
    case offline:
    {
      LOG_INFO(F("offline"));
      establish_connection();
      set_iff<run_level_t>(device_run_level, RL1);
      break;
    }
    case locked:
    {
      LOG_INFO(F("locked"));
      set_iff<run_level_t>(device_run_level, RL0);
      break;
    }
    case uncommissioned:
    {
      LOG_INFO(F("uncommissioned"));
      set_iff<run_level_t>(device_run_level, RL1);
      break;
    }
    default:
    {
      LOG_INFO(F("unknown"));
      FLOG_ERROR(F("Unknown operational status with value '%s'\r\n"), String(get<operational_status_t>(device_status)).c_str());

      set_iff<run_level_t>(device_run_level, RL0);
      break;
    }
  }

  // Send a 252 message informing of the changed operational status
  send_device_identification(0, NULL);
}

inline
void handle_run_level_change(task_id_t task_id, void* args)
{
  (void) task_id;
  (void) args;
  
  on_run_level_changed(get<run_level_t>(device_run_level, true));
}

void recover()
{
  LOG_ERROR(F("Entering recovery!"));
}

} // End of namespace marvin


using namespace marvin;

void setup()
{
  Serial.begin(115200);
  delay(100);

#if MARVIN_VERBOSITY_LEVEL > 0
  Serial.setDebugOutput(true);
#endif

  FLOG_INFO(F("\r\n\r\n----- Initializing Marvin Device -----\r\nVersion: %s\r\nBuild Date: %s\r\n--------------------------------------\r\n\r\n"), m_version, m_build_datetime);

  // Add a random delay during startup of the device.
  // This reduces the message storm when a network with many devices is powered up.
  delay(random(0, 250));

  if (!MARVINFS.begin())
  {
    LOG_WARN(F("Need to format the FS."));
    if (!MARVINFS.format())
    {
      LOG_ERROR(F("Error: Formatting failed."));
    }
  }

  EEPROM.begin(256);
  snprintf(const_cast<char*>(serial_number_str), 17, "%016llX", EEPROM.get<uint64_t>(0, serial_number_int));
  snprintf(const_cast<char*>(device_pin_str), 9, "%08X", EEPROM.get<uint32_t>(8, device_pin_int));
  EEPROM.end();

  const_cast<char*>(serial_number_str)[16] = 0;
  const_cast<char*>(device_pin_str)[8] = 0;

  FLOG_INFO(F("Device SN: %s\r\nDevice PIN: %s\r\n"), serial_number_str, device_pin_str);

#ifdef ESP8266
  randomSeed(analogRead(A0));
#endif

  ParameterBase::register_element<uint8_t> ("restart_counter",   restart_counter,   "",     0, true);
  ParameterBase::register_element<uint32_t>("hb_send_interval",  hb_send_interval,  "",     60000, true);
  ParameterBase::register_element<uint8_t> ("device_id",         device_id,         "",     0, true);
  ParameterBase::register_element<operational_status_t>("device_status", device_status, "", power_up, false);
  ParameterBase::register_element<run_level_t>("device_run_level", device_run_level, "", RL0, false);

  push_message_generator(252, &send_device_identification);
  define_onchange_task(&handle_state_change, device_status);
  set<run_level_t>(device_run_level, RL0); // Force a modified flag on this parameter (not set_iff)
  
  bus_setup();

  FLOG_INFO(F("\r\nDevice ID: %d\r\n"), get<uint8_t>(device_id));

  m_now = time(nullptr);
  m_now_ms = millis();

#ifdef ESP8266
  const rst_info* ri = ESP.getResetInfoPtr();
  FLOG_WARN(F("\r\nReset Reason: %d\r\n"), ri->reason);

  if (ri->reason == REASON_EXT_SYS_RST ||
      ri->reason == REASON_DEFAULT_RST)
  {
    set<unsigned char>(restart_counter, 0);
  }

  uint8_t rst_counter = get<unsigned char>(restart_counter);
#else
  uint8_t rst_counter = 0;
#endif


  define_onchange_task(&handle_run_level_change, device_run_level);

  set<operational_status_t>(device_status, (rst_counter >= 6) ?
                                            recovery : // Enter Recovery Mode
                                            offline); // This will trigger establishing a connection

  set<uint8_t>(restart_counter, ++rst_counter);

  define_delayed_task([](task_id_t, void*) -> void
  {
    set<uint8_t>(restart_counter, 0);
  }, 60000, false, NULL);

  if (!initialize())
  {
    LOG_ERROR(F("ERROR: Device initialization failed!"));
    //set<operational_status_t>(device_status, uninitialized);
  }

  enable_interrupts();

  ParameterBase::dump_parameters();

  register_message_generator(208, get<uint32_t>(hb_send_interval));

#ifdef ESP8266
  FLOG_INFO(F("Initialization complete.\r\nHeap size: %d\r\n"), ESP.getFreeHeap());
#endif

}

IRAM_ATTR
void loop()
{
  m_now = time(nullptr);
  m_now_ms = millis();

  bus_receive_and_handle();

  run_scheduled_tasks(m_now_ms);

  // @REVIEW Could this be removed in future?
  step();

#if MARVIN_VERBOSITY_TRACE
  if (step_counter >= 100000)
  {
    FLOG_TRACE(F("Performance: %dms/step /%dms max\r\n"), step_acc / step_counter, step_longest);

#ifdef ESP8266
    FLOG_TRACE(F("Heap size: %d\r\n"), ESP.getFreeHeap());
#endif

    step_longest = 0;
    step_acc = 0;
    step_counter = 0;
  }

  uint32_t dif = m_now_ms - last_step;
  step_acc += dif;
  if (dif > step_longest)
  {
    step_longest = dif;
  }

  ++step_counter;

  last_step = m_now_ms;
#endif
}
