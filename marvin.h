//// Copyright (C) 2020 Eric Diethelm

//// This program is free software: you can redistribute it and/or modify
//// it under the terms of the GNU General Public License as published by
//// the Free Software Foundation, either version 3 of the License, or
//// (at your option) any later version.

//// This program is distributed in the hope that it will be useful,
//// but WITHOUT ANY WARRANTY; without even the implied warranty of
//// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//// GNU General Public License for more details.

//// You should have received a copy of the GNU General Public License
//// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "globals.h"
#include "gpio.h"
#include "device.h"
#include "parameter.hpp"
#include "message_handling.h"
#include "scheduler.h"
#include "device_message.h"


namespace marvin
{

// ------------------------------------------------------------- //
//                                                               //
//                     Device Run-Levels                         //
//                                                               //
// ------------------------------------------------------------- //

/*
 * Run-Level 0
 *
 * No application-specific code is running:
 * - all timers are stopped
 * - any Interrupt is detached
 * - step() function is not called
 *
 * Operational stati related to this Run-Level:
 * - power_up
 * - maintenance
 * - locked
 * - any not defined value
 *
 *
 * Run-Level 1
 *
 * Limited application-specific code for autonomous operation is running.
 * No guarantees of a functioning network are given.
 * Should minimize interference with critical operations of the platform
 * (e.g. interrupts during an DUS update)
 *
 * Operational stati related to this Run-Level:
 * - uncommissioned
 * - recovery
 * - offline
 * - online
 *
 *
 * Run-Level 2
 *
 * Full operation of application-specific code with no limitations and
 * guarantee of a functioning network.
 *
 * Operational stati related to this Run-Level:
 * - operational
 *
 */


// ------------------------------------------------------------- //
//                                                               //
//                          System API                           //
//                                                               //
// ------------------------------------------------------------- //

#define VERSION \
  version()

#define BUILD_DATETIME \
  build_datetime()

#define SERIAL_NUMBER \
  serial_number()

#define DEVICE_PIN \
  device_pin()

#define DEVICE_STATUS \
  get<operational_status_t>(device_status)

#define DEVICE_ID \
  get<uint8_t>(device_id)

#define NOW_SEC \
  now()

#define NOW_MILLIS \
  now_ms()

#define restart() \
  restart()


// ------------------------------------------------------------- //
//                                                               //
//                    Parameter Handling                         //
//                                                               //
// ------------------------------------------------------------- //

template <typename T>
inline
void register_parameter (const char* name, uint16_t param_id, const char* description, const T& initial_value)
{
  ParameterBase::register_element<T>(name, param_id, description, initial_value, true);
}

template <typename T>
inline
void register_variable (const char* name, uint16_t param_id, const char* description, const T& initial_value)
{
  ParameterBase::register_element<T>(name, param_id, description, initial_value, false);
}

#define is_modified(param) \
  ParameterBase::is_modified(param) 

#define reset_modified(param) \
  ParameterBase::reset_modified(param)


// ------------------------------------------------------------- //
//                                                               //
//                       Sending Messaged                        //
//                                                               //
// ------------------------------------------------------------- //

#define send(message) \
  send(message)

#define send_on_condition(msg_id, condition, args) \
  define_oncondition_task([](task_id_t, void*) -> void {send(msg_id);}, [](void*) -> bool condition, args);

#define send_on_value_changed(msg_id, parameter, args...) \
  define_onchange_task([](task_id_t, void*) -> void {send(msg_id);}, parameter, ##args)

#define make_message(/* uint16_t msg_id, bool rtr */ args...) \
  Message(new DeviceMessage(DEVICE_ID, args))

// ------------------------------------------------------------- //
//                                                               //
//                      Message Processing                       //
//                                                               //
// ------------------------------------------------------------- //

template <typename T>
inline
void register_message_source(uint16_t msg_src, uint16_t container, T (*parser)(const Message))
{
  push_message_source<T>(msg_src, container, parser);
}

// ------------------------------------------------------------- //
//                                                               //
//                      Interrupt Handling                       //
//                                                               //
// ------------------------------------------------------------- //

#define define_interrupt_handler(name, body)   \
  IRAM_ATTR                                    \
  void name ()                                 \
  {                                            \
    if (!interrupts_enabled())                 \
    {                                          \
      return;                                  \
    }                                          \
                                               \
    body                                       \
  }
}
